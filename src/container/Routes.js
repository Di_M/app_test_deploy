import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'

import LPRoute from "../components/lib/Layout/LPRoute";
import AppRoute from "../components/lib/Layout/AppRoute";

import HomePage from "../components/HomePage";
import RegistrationPage from "../components/SignUpPage/RegistrationPage/RegistrationPage";
import SingUp from "../components/SignUpPage/ChooseRolePage/ChooseRolePage";
import SignInPage from "../components/SignInPage/SignInPage";
import ReestablishPasswordPage from "../components/ReestablishPasswordPage/ReestablishPasswordPage";
import AboutUs from "../components/AboutUsPage/AboutUs";
import ContactsPage from "../components/ContactsPage/ContactsPage";
import FaqPage from "../components/FaqPage/Faq";
import Course from "../components/CourseSection/Courses/Course/Course";
import Lesson from "../components/CourseSection/LessonSection/Lesson/Lesson";
import CoursesFullCourse from "../components/AdminPage/CoursesFullCourse/CoursesFullCourse";
import CoursesList from "../components/CourseSection/Courses/CoursesList/CoursesList"
import GeneralContent from "../components/AdminPage/GeneralContent/GeneralContent";
import CourseContent from "../components/AdminPage/CourseContent/CourseContent";
import MyChildrenContent from "../components/AdminPage/MyChildrenContent/MyChildrenContent";
import SettingsContent from "../components/AdminPage/SettingsContent/SettingsContent";
import AnalyticsContent from '../components/AdminPage/AnalyticsContent/AnalyticsContent';
import SubscriptionContent from "../components/AdminPage/SubscriptionContent/SubscriptionContent";
import AwardsContent from '../components/AdminPage/AwardsContent/AwardsContent';


export default Routes;

function Routes(props) {

    const {auth, role, id} = props;

    return (
        <Router>
            <Switch>
                <LPRoute 
                    exact 
                    path={'/'} 
                    auth={auth} 
                    userId={id} 
                    role={role} 
                    component={HomePage}/>
                <LPRoute 
                    exact 
                    path={'/about'} 
                    auth={auth} 
                    userId={id} 
                    role={role} 
                    component={AboutUs}/>
                <LPRoute 
                    exact 
                    path={'/contacts'} 
                    auth={auth} 
                    userId={id} 
                    role={role} 
                    component={ContactsPage}/>
                <LPRoute 
                    exact 
                    path={'/faq'} 
                    auth={auth} 
                    userId={id} 
                    role={role} 
                    component={FaqPage}/>
                <LPRoute 
                    exact 
                    path={'/sign-up'} 
                    auth={auth} 
                    userId={id} 
                    role={role} 
                    component={SingUp}/>
                <LPRoute 
                    exact 
                    path={'/sign-up/parent'} 
                    auth={auth} 
                    userId={id} 
                    role={role} 
                    component={RegistrationPage}/>
                <LPRoute 
                    exact 
                    path={'/sign-in'} 
                    auth={auth} 
                    userId={id} 
                    role={role} 
                    component={SignInPage}/>
                <LPRoute 
                    exact 
                    path={'/reestablish'} 
                    auth={auth} 
                    userId={id} 
                    role={role} 
                    component={ReestablishPasswordPage}/>
                <AppRoute 
                    exact
                    path={`/account/${id}`}
                    auth={auth}
                    role={role}
                    userId={id}
                    component={GeneralContent}/>
                <AppRoute 
                    exact
                    path={`/account/${id}/children`}
                    auth={auth}
                    role={role}
                    userId={id}
                    component={MyChildrenContent}/>
                <AppRoute 
                    exact
                    path={`/account/${id}/courses`}
                    auth={auth}
                    role={role}
                    userId={id}
                    component={CourseContent}/>
                <AppRoute 
                    exact
                    path={`/account/${id}/analytics`}
                    auth={auth}
                    role={role}
                    userId={id}
                    component={AnalyticsContent}/>
                <AppRoute 
                    exact
                    path={`/account/${id}/awards`}
                    auth={auth}
                    role={role}
                    userId={id}
                    component={AwardsContent}/>
                <AppRoute 
                    exact
                    path={`/account/${id}/subscriptions`}
                    auth={auth}
                    role={role}
                    userId={id}
                    component={SubscriptionContent}/>
                <AppRoute 
                    exact
                    path={`/account/${id}/settings`}
                    auth={auth}
                    role={role}
                    userId={id}
                    component={SettingsContent}/>
                {/* <AppRoute exact path={`/account/${id}/progress`} component={GeneralContent}/> */}



                <AppRoute 
                    exact 
                    path={`/account/${id}/courses/math`}
                    auth={auth}
                    role={role}
                    userId={id}
                    component={CoursesFullCourse}/>

                <AppRoute
                    exact
                    path={`/account/${id}/courses/math/lesson/:id`}
                    auth={auth}
                    role={role}
                    userId={id}
                    component={Lesson}/>

                {/*TEMPORARY COURSE/LESSON ROUTES*/}
                <LPRoute
                    exact
                    path={`/courseslist`}
                    auth={auth}
                    userId={id}
                    role={role}
                    component={CoursesList}/>
                <LPRoute
                    exact
                    path={`/:courseId`}
                    auth={auth}
                    userId={id}
                    role={role}
                    component={Course}/>
                <LPRoute
                    exact
                    path={`/:courseId/:lesson/:id`}
                    auth={auth}
                    userId={id}
                    role={role}
                    component={Lesson}/>
                
            </Switch>
        </Router>
    );
}