export const url = {

    signUP: 'http://localhost:4004/sign-up',
    signIn: 'http://localhost:4004/sign-in',
    changePassword: 'http://localhost:4004/change-password',
    signUpChild: 'http://localhost:4004/child/add',
    deleteChild: 'http://localhost:4004/child/',
    editChild: 'http://localhost:4004/child/edit/',
    assignCourse: 'http://localhost:4004/child/add-course',
    reassignCourse: 'http://localhost:4004/child/remove-course',
    userInfo: 'http://localhost:4004/account',
    coursesList: 'http://localhost:4004/course/list',
    fullCoursesList: 'http://localhost:4004/course/list/details',
    oneCourse: 'http://localhost:4004/course/view/',
    oneLessonView: 'http://localhost:4004/lesson/view/',
    oneLessonAnswer: 'http://localhost:4004/lesson//answer/',
    coursesDetails:'http://localhost:4004/course/list/details'
};
