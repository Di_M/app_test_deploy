import React from "react";
import {
    DashboardOutlined,
    RedditOutlined,
    ContactsOutlined,
    PieChartOutlined,
    VerifiedOutlined,
    SolutionOutlined,
    SettingOutlined,
    FundProjectionScreenOutlined
} from '@ant-design/icons';


export const parentDashboardMenu = [
    {title: 'My Board', linkName: '', icon: <DashboardOutlined />},
    {title: 'My children', linkName: '/children', icon: <RedditOutlined />},
    {title: 'Choose a course', linkName: '/courses', icon: <ContactsOutlined />},
    {title: 'Analytics', linkName: '/analytics', icon: <PieChartOutlined />},
    {title: 'Awards', linkName: '/awards', icon: <VerifiedOutlined />},
    {title: 'Subscriptions', linkName: '/subscriptions', icon: <SolutionOutlined />},
    {title: 'Settings', linkName: '/settings', icon: <SettingOutlined />}
];

export const childrenDashboardMenu = [
    {title: 'Course', linkName: '/courses', icon: <ContactsOutlined />},
    // {title: 'Progress', linkName: '/progress', icon: <FundProjectionScreenOutlined />},
    {title: 'Analytics', linkName: '/analytics', icon: <PieChartOutlined />},
    {title: 'Awards', linkName: '/awards', icon: <VerifiedOutlined />}
];