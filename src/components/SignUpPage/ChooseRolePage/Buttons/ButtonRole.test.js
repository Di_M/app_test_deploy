import React from 'react';
import { shallow } from 'enzyme';
import ButtonRole from './ButtonRole';

describe('Button role ', () => {

    const setUp = (method, props = {}) => {
        const component =  method(<ButtonRole {...props}/>);
        return component;
    };

    describe('Have props', () => {

        let wrapper;
        const props = {
            icon: <i className="fa"/>,
            roleName: 'Role',
            route: 'goto.com',
            disabled: true
        };

        beforeEach(() => {
            wrapper = setUp(shallow, props);
        });

        it('Should render a button', () => {
            const component = wrapper.find('.btn-role');
            expect(component.length).toBe(1);
        });

        it('Should render an icon', () => {
            const component = wrapper.find('i');
            expect(component.hasClass('fa')).toEqual(true);
        });

        it('Should render a role name', () => {
            const component = wrapper.find('.btn-role-link');
            expect(component.text()).toEqual(props.roleName);
        });

        it('Should render a link with route', () => {
            const component = wrapper.find('NavLink');
            expect(component.prop('to')).toBe(props.route);
        });

        it('Should check disabled button', () => {
            const component = wrapper.find('.btn-role');
            expect(component.prop('disabled')).toEqual(props.disabled);
        });
    });
});