import React from 'react';
import "./RegistrationPage.less";
import {routes} from "../../../constants/constRouters";
import RegistrationForm from "./RegistrationForm/RegistrationForm";
import TextWithTransferTo from "../../lib/Navigation/TextWithTransferTo";
import LoginWithSocialButton from "../../lib/Button/LoginWithSocialButton";
import {GooglePlusOutlined, FacebookOutlined} from '@ant-design/icons';


const RegistrationPage = () => {

    const gmail = <GooglePlusOutlined />;
    const facebook = <FacebookOutlined />;

    return (
        <div className="registration">

            {/*Content*/}
            <div className="registration-content">

                {/*Title block*/}
                <h2 className="registration-title">
                    Create Account
                </h2>

                {/*Form*/}
                <div className="registration-form-wrap">
                    <RegistrationForm />
                </div>

                {/*OR Text*/}
                <div className="registration-another-way-title">
                    <span>OR</span>
                </div>

                {/*Social block*/}
                <div className="registration-social">
                    <LoginWithSocialButton
                        title="Sin Up with Gmail"
                        icon={gmail}
                        href="#"
                        color="error"/>
                    <LoginWithSocialButton
                        title="Sin Up with Facebook"
                        icon={facebook}
                        href="#"
                        color="info"/>
                </div>

                {/*Transfer to ...*/}
                <TextWithTransferTo
                    text="Do you have account? Please "
                    route={routes.signIn.href}
                />
            </div>
        </div>
    );
};

export default RegistrationPage;