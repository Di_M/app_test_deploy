import React from 'react';
import './Loader.less';

const Loader = () => {
    console.log('run');
    return (
        <div className="box">
            <h1 className="title">Loading</h1>
            <div className="rainbow-marker-loader">

            </div>
        </div>
    );
};

export default Loader;