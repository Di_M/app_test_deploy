import React, { useState, useEffect, useCallback } from "react";
import axios from "axios";
import { useHistory } from "react-router-dom";
import { Route, Redirect } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { Layout } from "antd";
import { routes } from "../../../constants/constRouters";
import LocalStorageRequest from "../../../services/LocalStorageRequest";
import LocalStorage from "../../../services/LocalStorageRequest";
import convertLessonId from "../../../services/helpers/ConvertLessonId";
import SignOutButton from "../Button/SignOutButton";
import * as TokenActions from "../../../actions/TokenActions";
import * as UserIDActions from "../../../actions/UserIDActions";
import * as RoleActions from "../../../actions/RoleActions";
import * as MyChildrenActions from "../../../actions/MyChildrenActions";
import * as CourseDetailsActions from "../../../actions/CourseDetailsActions";
import * as CourseListActions from "../../../actions/CourseListActions";
import * as LessonActions from "../../../actions/LessonActions";

import GeneralApi from '../../../services/generalApi';
import { getCourseListRequest, getCourseDetailsRequest } from "../../../services/courseApi/courseApi"
import AdminMenu from '../../AdminPage/AdminMenu/AdminMenu';
import { parentDashboardMenu, childrenDashboardMenu } from '../../../constants/constDashboardMenu';
import Loader from '../General/Loader';

import useLessonsUpdate from '../../../services/hooks/updateLessonId'

import './AppRoute.less';

export default AppRoute;

function AppRoute({
    render,
    component,
    auth,
    userId,
    role,
    path,
    ...routeProps
}) {
    console.log ('APP ROUTE COMPONENT IS RENDERING')
    const history = useHistory();
    const dispatch = useDispatch();
    
    const [userInfo, setUserInfo] = useState(null);
    const [loading, setLoading] = useState(false);

    let menuList = null;

    if(role === 'parent') {
        menuList = parentDashboardMenu;
    } else if(role === 'child') {
        menuList = childrenDashboardMenu;
    }

    const { Header, Content, Footer } = Layout;

    const onLogOut = () => {
        LocalStorageRequest.clearAllData();
        dispatch(TokenActions.deleteToken());
        dispatch(UserIDActions.deleteUserID());
        dispatch(RoleActions.deleteRole());
        dispatch(MyChildrenActions.clearChildren());
        dispatch(CourseDetailsActions.clearCourseDetails());
        dispatch(CourseListActions.clearCourseList());
        dispatch(LessonActions.clearLessonIdList());
        history.push(routes.home.href);
    };

    useEffect(()=>{
        const CancelToken = axios.CancelToken;
        const source = CancelToken.source();

        const user_id = LocalStorage.getData('userID');

        const loadData = () =>{
            try {
                GeneralApi.getUserInfo(user_id)
                    .then(({data: response}) => setUserInfo(response));
                GeneralApi.getUserChildren(user_id)
                    .then(({data: response}) =>  dispatch(MyChildrenActions.getChildren(response.children)));
                getCourseListRequest()
                    .then(response => dispatch(CourseListActions.displayCourseList(response)));
                getCourseDetailsRequest()
                    .then(response => dispatch(CourseDetailsActions.addCourseDetails(response)));
                //Loading animation turn off
                setLoading(false);

            }catch(error){
                console.error(error);
                //Loading animation turn off
                setLoading(false);
            }
        };
        loadData();
        return () => {
            source.cancel();
        };


    },[]);


        // const getCourseDetails  = useSelector(state => state.CourseDetailsReducer);
        // const courseData = getCourseDetails.courseDetails;
        // console.log('Layout=>AppRoute.js, current Course Data from CourseDetailsReducer:', courseData);
        // // Create a list of Lessons Id
        //
        // let userIdList = convertLessonId(courseData);
        // const updateLessonListStorage =()=>{
        //     dispatch(LessonActions.addLessonIdList(userIdList));
        // };
        // if (userIdList !==undefined){updateLessonListStorage()}

    // moved to Hook:
    useLessonsUpdate()



    const renderLayout = () => (
        <main>

            {loading && <Loader />}

            <Layout style={{ minHeight: '100vh' }}>
                <AdminMenu id={userId} menuList={menuList} path={path}/>
                <Layout className="admin-content-layout">
                    <Header className="admin-content-header">
                    <div className="admin-content-header-title">
                        Welcome, dear { userInfo ? userInfo.name : 'User' }
                    </div>
                    <div className='admin-content-header-btn-block'>
                        <SignOutButton
                            title={"Sign Out"}
                            color={"info"}
                            onClick={onLogOut}
                        />
                    </div>
                    </Header>
                    <Content style={{ margin: '0 16px' }}>
                        <div className="admin-content-main">
                            {render 
                                ? render()
                                : React.createElement(component)}
                        </div>
                    </Content>
                    <Footer style={{ textAlign: 'center', padding: '10px 50px' }}> ©2020 Created by Team</Footer>
                </Layout>
                
            </Layout>
        </main>
    );

    return (
        <Route
            {...routeProps} 
            render={() => (
                auth
                ? renderLayout()
                : <Redirect to={`${routes.signIn.href}`} />
            )}
        />
    );
};