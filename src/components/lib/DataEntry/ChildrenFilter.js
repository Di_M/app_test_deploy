import React from 'react';
import {Select} from "antd";
import './ChildrenFilter.less';

const { Option } = Select;


const ChildrenFilter = (props) => {

    const {onChange, options, title} = props;

    return (
        <div className="children-filter">
            <div className="children-filter-title">{title}</div>
            <Select
                defaultValue="all"
                style={{width: 120}}
                onChange={onChange}
            >
                {options}

                <Option value="all">All</Option>
            </Select>
        </div>
    );
};

export default ChildrenFilter;