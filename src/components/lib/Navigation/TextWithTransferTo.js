import React from "react";
import {NavLink} from 'react-router-dom';

import "./TextWithTransferTo.less";

const TextWithTransferTo = (props) => {

    const {text, route} = props;

    return (
        <div >
            <p className="transfer-to">
                <span>{text}</span>
                <NavLink to={route} exact>click here</NavLink>
            </p>
        </div>
    );
};

export default TextWithTransferTo;

