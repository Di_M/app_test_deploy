import React from 'react';
import { shallow } from 'enzyme';
import TextWithTransferTo from './TextWithTransferTo';

describe('Link "click here" ', () => {

    const setUp = (method, props = {}) => {
        const component =  method(<TextWithTransferTo {...props}/>);
        return component;
    };

    describe('Have props', () => {

        let wrapper;
        const props = {
            text: 'Some text',
            route: 'go.com',
        };

        beforeEach(() => {
            wrapper = setUp(shallow, props);
        });

        it('Should render a paragraph', () => {
            const component = wrapper.find('.transfer-to');
            expect(component.length).toBe(1);
        });

        it('Should render a title', () => {
            const component = wrapper.find('span');
            expect(component.length).toBe(1);
        });

        it('Should render a link with route', () => {
            const component = wrapper.find('NavLink');
            expect(component.prop('to')).toBe(props.route);
        });
    });
});