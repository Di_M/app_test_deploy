import React from "react";
import {NavLink} from 'react-router-dom';
import './NavLinks.less'
import {routes} from '../../../constants/constRouters';

const NavLinks  = props => {

    return <React.Fragment>
    <ul className="nav-links">
        <li><NavLink to="/about" exact>About</NavLink></li>
        <li><NavLink to="/contacts" exact>Contacts</NavLink></li>
        <li><NavLink to="/faq" exact>FAQ</NavLink></li>
        <li><NavLink to={routes.signUp.href} exact>{routes.signUp.name}</NavLink></li>
        <li><NavLink to={routes.signIn.href} exact>{routes.signIn.name}</NavLink></li>
    </ul>
    </React.Fragment>
}
export default  NavLinks;