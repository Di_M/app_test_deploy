import React from 'react';
import {Modal, Form} from 'antd';
import FormAddCreditCard from '../DataEntry/FormAddCreditCard';


const ModalAddCard = (props) => {

    const { visible, onCreate, onCancel, cardValue, title, okText } = props;

    const [form] = Form.useForm();

    const validateForm = () => {

        form.validateFields()
            .then(values => {
                onCreate(values);
                form.resetFields();
            })
            .catch(info => {
                console.log('Validate Failed:', info);
            });
    };


    return (

        <Modal
            visible={visible}
            title={title}
            okText={okText}
            cancelText="Cancel"
            onCancel={() => {
                form.resetFields();
                onCancel();
            }}
            onOk={() => {
                validateForm();
            }}
        >

            <FormAddCreditCard formCreditCard={form} dataValues={cardValue} modalVisibility={visible}/>

        </Modal>
    );
};

export default ModalAddCard;
