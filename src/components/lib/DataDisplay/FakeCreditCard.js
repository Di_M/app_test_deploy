import React from 'react';
import './FakeCreditCard.less';
import { Tooltip } from 'antd';


const FakeCreditCard = (props) => {

    const {nameCard, firstNum, secondNum, thirdNum, lastNum, nameHolder, cardMonth, cardYear, onDelete, onEdit} = props;

    const addZero = (value) => {

        if(value.toString().length === 1) {
            return `0${value}`;
        }
        return value;
    };

    if(!nameHolder) {
        return null
    }

    return (
        <div className="subscriptions-card">

            <div className="subscriptions-card-title">
                {nameCard}
            </div>

            <div className="subscriptions-card-number">
                <div className="subscriptions-card-number-split">
                    {firstNum}
                </div>
                <div className="subscriptions-card-number-split">
                    {secondNum}
                </div>
                <div className="subscriptions-card-number-split">
                    {thirdNum}
                </div>
                <div className="subscriptions-card-number-split">
                    {lastNum}
                </div>
            </div>

            <div className="subscriptions-card-holder">
                {nameHolder.toUpperCase()}
            </div>

            <div className="subscriptions-card-term">
                {`${addZero(cardMonth)} / ${cardYear}`}
            </div>

            <Tooltip title="Delete your card">
                <button className="subscriptions-card-delete-btn" onClick={onDelete}>
                    X
                </button>
            </Tooltip>

            <Tooltip title="Edit your card">
                <button className="subscriptions-card-edit-btn" onClick={onEdit}>
                    edit
                </button>
            </Tooltip>
        </div>
    );

};

export default FakeCreditCard;