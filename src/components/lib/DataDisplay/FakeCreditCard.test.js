import React from 'react';
import { shallow } from 'enzyme';
import FakeCreditCard from './FakeCreditCard';

describe('Subs Credit Card block', () => {

    const setUp = (method, props = {}) => {
        const component =  method(<FakeCreditCard {...props}/>);
        return component;
    };

    describe('Have props', () => {

        let component;
        const moskDelFunc = jest.fn();
        const moskEditFunc = jest.fn();
        const props = {
            nameCard: 'Name',
            firstNum: 1223,
            secondNum: 1212,
            thirdNum: 2323,
            lastNum: 2323,
            nameHolder: 'Derek Shark',
            cardMonth: 12,
            cardYear: 12,
            onDelete: moskDelFunc,
            onEdit: moskEditFunc
        };

        beforeEach(() => {
            component = setUp(shallow, props);
        });

        it('Should render a container', () => {
            const wrapper = component.find('.subscriptions-card');
            expect(wrapper.length).toBe(1);
        });

        it('Should render a Title', () => {
            const wrapper = component.find('.subscriptions-card-title');
            expect(wrapper.length).toBe(1);
        });

        it('Should render a Card number', () => {
            const wrapper = component.find('.subscriptions-card-number-split');
            expect(wrapper.length).toBe(4);
        });

        it('Should render a Card holder', () => {
            const wrapper = component.find('.subscriptions-card-holder');
            expect(wrapper.text()).toEqual(`${props.nameHolder.toUpperCase()}`);
        });

        it('Should render a card term with card`s month and year', () => {
            const wrapper = component.find('.subscriptions-card-term');
            expect(wrapper.text()).toEqual(`${props.cardMonth} / ${props.cardYear}`);
        });

        it('Should emit a callback on click event Delete button', () => {
            const button = component.find('.subscriptions-card-delete-btn');
            button.simulate('click');
            const callback = moskDelFunc.mock.calls.length;
            expect(callback).toBe(1);
        });

        it('Should emit a callback on click event Edit button', () => {
            const button = component.find('.subscriptions-card-edit-btn');
            button.simulate('click');
            const callback = moskEditFunc.mock.calls.length;
            expect(callback).toBe(1);
        });

    });

    describe('Have NOT props', () => {

        let component;

        beforeEach(() => {
            component = setUp(shallow);
        });

        it('Should not render component', () => {
            const wrapper = component.find('.subscriptions-card');
            expect(wrapper.length).toBe(0);
        });
    });

});