import React from "react";
import {CaretRightOutlined, CheckCircleOutlined, ScheduleOutlined, ThunderboltOutlined} from "@ant-design/icons";
import {Collapse, Statistic} from "antd";
import './ChildrenTotalStatistics.less';

const { Panel } = Collapse;

const ChildrenTotalStatistics = (props) => {

    const {totalScoreLesson, totalQuizPercent, totalPoints} = props;

    return (
        <Collapse
            className="children-total-statistics"
            // defaultActiveKey={['1']}
            bordered={false}
            expandIcon={({ isActive }) => <CaretRightOutlined rotate={isActive ? 90 : 0} />}
        >
            <Panel className="children-total-statistics-panel"
                   header="Total Score"
                   key="1"
            >
                <div className="children-total-statistics-data">
                    <div>
                        <Statistic
                            title="Finished lessons:"
                            value={totalScoreLesson || 0}
                            prefix={<ScheduleOutlined style={{ fontSize: '30px', color: '#08c' }}/>}
                        />
                    </div>
                    <div>
                        <Statistic
                            title="Quiz score:"
                            value={totalQuizPercent || 0.00}
                            precision={2}
                            prefix={<ThunderboltOutlined style={{ fontSize: '30px', color: '#ba8b00' }}/>}
                            suffix="%"
                        />
                    </div>
                    <div>
                        <Statistic
                            title="Points:"
                            value={totalPoints || 0}
                            prefix={<CheckCircleOutlined style={{ fontSize: '30px', color: '#52c41a' }} />}
                        />
                    </div>
                </div>
            </Panel>
        </Collapse>
    );
};

export default ChildrenTotalStatistics;