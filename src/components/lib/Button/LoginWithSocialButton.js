import React from 'react';
import {Button} from "antd";
import './LoginWithSocialButton.less';

const LoginWithSocialButton = (props) => {

    const {title, icon, href, color} = props;

    return(
        <Button
            className={"login-social-btn " + color}
            type="primary"
            size="large"
            href={href}
            icon={icon}
            disabled={true}
        >
            {title}
        </Button>
    );

};

export default LoginWithSocialButton;