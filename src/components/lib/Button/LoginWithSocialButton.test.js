import React from 'react';
import { render, shallow } from 'enzyme';
import LoginWithSocialButton from './LoginWithSocialButton';


describe('Social button', () => {

    const setUp = (method, props) => {
       const component =  method(<LoginWithSocialButton {...props}/>);
       return component;
    };

    const props = {
        title: 'Some',
        icon: <i className="fas"/>,
        href: 'go.com',
        color: 'black'
    };

    describe('Test text props', () => {

        const nextProps = {
           ...props,
           title: 'Go to...'
        };

        it('Should render correct name', () => {
            const component = setUp(render, nextProps);
            expect(component.text()).toEqual('Go to...');
        });

    });

    describe('Test icon props', () => {

        const testIcon = <i className='fa'/>;
        const nextProps = {
            ...props,
            icon: testIcon
        };

        it('Should render icon', () => {
            const component = setUp(shallow, nextProps);
            const wrapper = component.find('.login-social-btn');
            expect(wrapper.length).toBe(1);
        });
    });

    describe('Test href props', () => {

        const nextProps = {
            ...props,
            href: 'google.com'
        };

        it('Should render icon', () => {
            const component = setUp(shallow, nextProps);
            const wrapper = component.find('.login-social-btn');
            expect(wrapper.prop('href')).toBe('google.com');
        });
    });

    describe('Test color className props', () => {

        const nextProps = {
            ...props,
            color: 'primary'
        };

        it('Should render className from props', () => {
            const component = setUp(render, nextProps);
            expect(component.hasClass('primary')).toEqual(true);
        });
    });

});