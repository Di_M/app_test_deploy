import React from 'react';
import { render, shallow, mount } from 'enzyme';
import SignOutButton from './SignOutButton';


describe('Sign Out button', () => {

    const setUp = (method, props) => {
        const component =  method(<SignOutButton {...props}/>);
        return component;
    };

    const props = {
        title: 'Some',
        color: 'black',
        // href: 'go.com',
        onClick: () => {}
    };

    describe('Test text props', () => {

        const nextProps = {
            ...props,
            title: 'Out of'
        };

        it('Should render correct name', () => {
            const component = setUp(render, nextProps);
            expect(component.text()).toEqual('Out of');
        });

    });

    describe('Test color className props', () => {

        const nextProps = {
            ...props,
            color: 'primary'
        };

        it('Should render className from props', () => {
            const component = setUp(render, nextProps);
            expect(component.hasClass('primary')).toEqual(true);
        });
    });

    // describe('Test href props', () => {
    //
    //     const nextProps = {
    //         ...props,
    //         href: 'gggg.com'
    //     };
    //
    //     it('Should render href', () => {
    //         const component = setUp(shallow, nextProps);
    //         const wrapper = component.find('.logout-btn');
    //         expect(wrapper.prop('href')).toBe('gggg.com');
    //     });
    // });

    describe('Test onClick props', () => {

        const onButtonClick = jest.fn();
        const nextProps = {
            ...props,
            onClick: onButtonClick
        };

        it('Should simulate click', () => {
            const component = setUp(mount, nextProps);
            component.props().onClick();
            expect(onButtonClick).toHaveBeenCalled();
        });
    });

});