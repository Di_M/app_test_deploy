import React from 'react'
import './FirstScreen.less'
import {Button} from 'antd'
import school_kid from './images/school_kid.png'

const FirstScreen = (props)=>{
    return  (<React.Fragment>
        <section className="first-screen-block">
            <div className="first-screen-left-side">
                <div className="first-screen-left-side-text">
                <h1 className="first-screen-left-side-text-title">Your child is a genius. Lets us prove this!</h1>
                <p className="first-screen-left-side-text-sub-title">We help children discover their potential</p>
                </div>
                <div className="first-screen-left-side-buttons">
                    <Button href="/"   type="primary" className="first-screen-left-side-btn">Try for Free</Button>
                    <Button  href="/" type="default"  ghost className="first-screen-left-side-btn first-screen-left-side-btn-login">Login</Button>
                </div>
                </div>
            <div className="first-screen-right-side">
                <img alt="your child" className="first-screen-right-side-img" src={school_kid}/>
            </div>


        </section>
    </React.Fragment>)
}
export default FirstScreen;