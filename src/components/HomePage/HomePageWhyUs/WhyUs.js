import React from 'react'
import './WhyUs.less'
import {
    LikeOutlined,
    AreaChartOutlined,
    SmileOutlined,
    TrophyOutlined,
    UserAddOutlined,
    StarOutlined,
} from '@ant-design/icons';

const WhyUs = props=>{

    return ( <React.Fragment>
        <section className="section-why-us">
            <h2 className="section-why-us-section-title">Why Choose our services</h2>
            <div className="section-why-us-advantages">

                <div className="section-why-us-item">
                    <div className="section-why-us-item-icon">
                        <LikeOutlined className="section-why-us-item-icon-img" />
                    </div>
                    <h4 className="section-why-us-item-title">High quality educational programs </h4>
                </div>

                <div className="section-why-us-item">
                    <div className="section-why-us-item-icon">

                        <AreaChartOutlined className="section-why-us-item-icon-img" />
                    </div>
                    <h4 className="section-why-us-item-title">Tracking Learning process </h4>
                </div>

                <div className="section-why-us-item">
                    <div className="section-why-us-item-icon">
                        <SmileOutlined className="section-why-us-item-icon-img" />
                    </div>
                    <h4 className="section-why-us-item-title">Convenient Usage</h4>
                </div>

                <div className="section-why-us-item">
                    <div className="section-why-us-item-icon">

                        <TrophyOutlined className="section-why-us-item-icon-img" />
                    </div>
                    <h4 className="section-why-us-item-title">Gamification mechanics</h4>
                </div>

                <div className="section-why-us-item">
                    <div className="section-why-us-item-icon">

                        <UserAddOutlined className="section-why-us-item-icon-img" />
                    </div>
                    <h4 className="section-why-us-item-title">Individual Approach</h4>
                </div>

                <div className="section-why-us-item">
                    <div className="section-why-us-item-icon">

                        <StarOutlined className="section-why-us-item-icon-img" />
                    </div>
                    <h4 className="section-why-us-item-title">Tests, quizzes and games</h4>
                </div>

            </div>

        </section>
    </React.Fragment>)
}
export default WhyUs