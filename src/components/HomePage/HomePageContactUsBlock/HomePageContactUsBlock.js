import React from "react";
import './HomePageContactUsBlock.less'
import {Button} from 'antd'

const HomePageContactUsBlock = props=>{

    return(
        <React.Fragment>
            <section className="section-contact-us">
                <div className="section-contact-us-block">
                    <div className="section-contact-us-title">
                        <h2>Do you want to know more?</h2>
                    </div>
                    <div className="section-contact-us-button">
                       <Button  href="/"   type="primary" className="write-to-us-btn">Write us</Button>
                    </div>
                </div>
            </section>
        </React.Fragment>
    )
}

export default HomePageContactUsBlock