import React from 'react';
import MainNavigation from '../lib/Navigation/MainNavigation'
import './index.less';
import FirstScreen from "./FirstScreen/FirstScreen";
import HomePagePrograms from "./HomePagePrograms/HomePagePrograms";
import WhyUs from "./HomePageWhyUs/WhyUs";
import Testimonials from './Testimonials/Testimonials'
import HomePageContactUsBlock from './HomePageContactUsBlock/HomePageContactUsBlock'
import Footer from "./Footer/Footer";

function HomePage() {
    console.log("HomePage");
    
    return (
        <>
            <FirstScreen/>
            <HomePagePrograms/>
            <WhyUs/>
            <Testimonials/>
            <HomePageContactUsBlock/>
        </>
    );
}

export default HomePage;