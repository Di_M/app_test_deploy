import React, { Component } from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import './TestimonialsSlider.less'

import Lesley from './images/Lesley.jpg'
import Rooney from './images/Rooney.jpg'
import Dick from './images/Dick.jpg'
import Rebeca from './images/Rebeca.jpg'


function SampleNextArrow(props) {
    const { className, style, onClick } = props;
    return (
        <div
            className={className}
            style={{ ...style, display: "block", background: "#008c99" }}
            onClick={onClick}
        />
    );
}

function SamplePrevArrow(props) {
    const { className, style, onClick } = props;
    return (
        <div
            className={className}
            style={{ ...style, display: "block", background: "#008c99" }}
            onClick={onClick}
        />
    );
}

export default class TestimonialsSlider extends Component {
    render() {
        const settings = {
            dots: true,
            infinite: true,
            slidesToShow: 3,
            slidesToScroll: 1,
            nextArrow: <SampleNextArrow />,
            prevArrow: <SamplePrevArrow />,
            responsive: [
                {
                    breakpoint: 1124,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        initialSlide: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]

        };
        return (
            <div>
                <Slider {...settings}>
                    <div>
                        <div className="slider-testimonials-item">
                            <div className="slider-testimonials-item-quote">
                                <blockquote> <q>
                                    Elementary Skills significantly helped my children in learning Math and English.
                                    I think it is the best solution on the market and strongly recommend to use it for learning
                                    basic concepts of math and other subjects.
                                </q></blockquote>
                            </div>
                            <div className="slider-testimonials-item-author">
                                <div className="slider-testimonials-author-image">
                                    <img src={Rooney} alt="author" />
                                </div>
                                <div className="slider-testimonials-author-meta">
                                    <p>Ronney</p>
                                    <span>CEO & Founder -Dhoom Inc</span>
                                </div>

                            </div>
                        </div>

                    </div>
                    <div>
                        <div className="slider-testimonials-item">
                            <div className="slider-testimonials-item-quote">
                                <blockquote> <q>
                                    The most attractive courses for your child.
                                    Forbes recommends this resource is a Must have in your educational collection.
                                     My child learned basic calculation in 5 days. I think it's a great result. Now we are testing
                                    a logic course.
                                </q></blockquote>
                            </div>
                            <div className="slider-testimonials-item-author">
                                <div className="slider-testimonials-author-image">
                                    <img src={Lesley} alt="author" />
                                </div>
                                <div className="slider-testimonials-author-meta">
                                    <p>David Lesley</p>
                                    <span>Chief Editor at Forbes</span>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div>
                        <div className="slider-testimonials-item">
                            <div className="slider-testimonials-item-quote">
                                <blockquote> <q>
                                    Great partners. These guys know everything about education and really love their work.
                                    I am proud to be one of their partners and invest our resources in developing cutting edge educational
                                    resources. My advice - never stop dreaming!
                                </q></blockquote>
                            </div>
                            <div className="slider-testimonials-item-author">
                                <div className="slider-testimonials-author-image">
                                    <img src={Dick} alt="author" />
                                </div>
                                <div className="slider-testimonials-author-meta">
                                    <p>Dick McGregory</p>
                                    <span>School Principal, Washington DC</span>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div>
                        <div className="slider-testimonials-item">
                            <div className="slider-testimonials-item-quote">
                                <blockquote> <q>
                                    We have tried many solutions, however we couldn't find better than Elementary skills.
                                    Right now we consider to include lessons from this resource to our own educational programs,
                                    that will be beneficial for our pupils. Guys!
                                </q></blockquote>
                            </div>
                            <div className="slider-testimonials-item-author">
                                <div className="slider-testimonials-author-image">
                                    <img src={Rebeca} alt="author" />
                                </div>
                                <div className="slider-testimonials-author-meta">
                                    <p>Rebeca</p>
                                    <span>CEO & Founder - Students Inc</span>
                                </div>

                            </div>
                        </div>
                    </div>

                </Slider>
            </div>
        );
    }
}

