import React from 'react'
import TestimonialsSlider from './TestimonialsSlider'
import './Testimonials.less'


const Testimonials =props=>{

    return(
        <React.Fragment>
            <section className="section-testimonials">
                <h2 className="section-testimonials-title">What our customers say</h2>
  <div className="section-testimonials-slider">
  <TestimonialsSlider/>
  </div>

   </section>

    </React.Fragment>)
}
export default Testimonials