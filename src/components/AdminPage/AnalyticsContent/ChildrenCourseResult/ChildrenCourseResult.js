import React from "react";
import {Collapse} from "antd";
import {CaretRightOutlined} from "@ant-design/icons";
import './ChildrenCourseResult.less';
import ChildrenCourseResultItem from './ChildrenCourseResultItem/ChildrenCourseResultItem';

const { Panel } = Collapse;

const ChildrenCourseResult = (props) => {

    const {coursesList} = props;


    const coursesChildrenList = coursesList.map((item,i) => (
        <ChildrenCourseResultItem
            key={i}
            titleCourse={item.name}
            levelCourse={item.level}
            finishedLessons={item.courseScoreLessons}
            quizScore={item.courseQuizPercent}
            points={item.coursePoints}
        />
    ));

    return (
        <Collapse
            className="children-courses-result"
            bordered={false}
            expandIcon={({ isActive }) => <CaretRightOutlined rotate={isActive ? 90 : 0} />}
        >
            <Panel className="children-courses-result-panel"
                   header="Statistic by courses"
                   key="1"
            >
                {coursesChildrenList}
            </Panel>
        </Collapse>
    );
};

export default ChildrenCourseResult;
