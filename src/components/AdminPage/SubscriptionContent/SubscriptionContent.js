import React, {useState, useEffect} from 'react';
import './SubscriptionContent.less';
import {Button} from "antd";
import ModalAddCard from '../../lib/Modal/ModalAddCard';
import FakeCreditCard from '../../lib/DataDisplay/FakeCreditCard';
import {useDispatch, useSelector} from "react-redux";
import LocalStorageRequest from '../../../services/LocalStorageRequest';
import * as CreditCardActions from '../../../actions/CreditCardActions';

const SubscriptionContent = () => {

    const [showModal, setShowModal] = useState(false);
    let creditCardObj = useSelector(store => store.CreditCardReducer);
    const dispatch = useDispatch();

    useEffect( () => {
            const array = LocalStorageRequest.getData('creditCard');

            if (array && array.length !== 0) {
                dispatch(CreditCardActions.getCreditCard(array));
            }
    }, []);

    const onAddCard = (values) => {
        setShowModal(false);

        console.log('Received values of form: ', values);

        LocalStorageRequest.setData('creditCard', values);
        dispatch(CreditCardActions.getCreditCard(values));
    };

    const onShowModal = () => {
        setShowModal(!showModal);
    };

    const onDeleteCreditCard = () => {
        LocalStorageRequest.clearData('creditCard');
        dispatch(CreditCardActions.deleteCreditCard());
    };

    return (

        <div className="subscriptions">

            <h2 className="subscriptions-title">
                Subscriptions
            </h2>

            <div className="subscriptions-content-block">
                <p className="subscriptions-content-description">
                    Attention! Currently you are using trial of our product. Trial period will be finished in 30 days.
                </p>
                <p className="subscriptions-content-description">
                    Please add your credit card in order to continue work with our service after a trial period
                </p>

                {/* Button for call a modal */}
                { !(creditCardObj.added)
                    &&
                    < Button
                        className="subscriptions-content-btn"
                        type="primary"
                        onClick={onShowModal}
                    >
                        Add new card
                    </Button>
                }

                {/* A Modal "Add Credit Card" */}
                {!(creditCardObj.added)
                    ?
                    <ModalAddCard
                        title={"Add your card"}
                        okText={"Add card"}
                        visible={showModal}
                        onCreate={onAddCard}
                        onCancel={onShowModal}
                    />
                    :
                    <ModalAddCard

                        title={"Edit your card"}
                        okText={"Edit card"}
                        visible={showModal}
                        onCreate={onAddCard}
                        onCancel={onShowModal}
                        cardValue={creditCardObj.creditCard}
                    />
                }

                {/*Display Credit Card*/}

                <div className="subscriptions-card-block">
                    <div className="subscriptions-card-block-title">
                        Your card
                    </div>
                </div>

                {   (creditCardObj.added)
                    ?
                    <>
                        <FakeCreditCard
                            nameCard={creditCardObj.creditCard.cardName}
                            firstNum={creditCardObj.creditCard.cardNumFirst}
                            secondNum={creditCardObj.creditCard.cardNumSecond}
                            thirdNum={creditCardObj.creditCard.cardNumThird}
                            lastNum={creditCardObj.creditCard.cardNumLast}
                            nameHolder={creditCardObj.creditCard.nameHolder}
                            cardMonth={creditCardObj.creditCard.cardMonth}
                            cardYear={creditCardObj.creditCard.cardYear}
                            onDelete={onDeleteCreditCard}
                            onEdit={onShowModal}
                        />
                    </>
                    :
                    <p className="subscriptions-card-block-empty">
                        You don`t have any cards.
                    </p>
                }



                {/*Payment history*/}
                <div className="subscriptions-payment-history">
                    <div className="subscriptions-payment-history-title">
                        Payment history
                    </div>

                    <p className="subscriptions-payment-history-empty">
                        You have no active subscriptions. Please create one.
                    </p>

                </div>
            </div>


        </div>
    );
};

export default SubscriptionContent;
