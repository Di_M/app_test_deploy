import React from 'react';
import {useSelector} from "react-redux";
import { Card } from "antd";

import './GeneralContent.less';

const GeneralContent = () => {

  const children = useSelector(store => store.MyChildrenReducer);
  // courses - to be implemented

    return (
        <div className="general-content">
          <h2 className="general-content-title">
            General Information
          </h2>
          <div className="general-content-block">
            <p className="general-content-description">Welcome on-board! Ready to work hard? Ha-ha, don't worry - education is fun:)
              Together we are going to discover a whole new world full of interesting facts and exciting challenges.
            </p>
            <p className="general-content-description">
              Here you can add new courses, create accounts for your children and track their progress.
              To begin - find all necessary tabs to the left. Will be fun - we promise;)
            </p>
            <ul className="general-content-info-menu">
              <li className="general-content-info-menu-item">
                <Card style={{ width: 200 }}>
                  <p className="general-content-item-text"> Your Courses: <span>2</span></p>
                </Card>
              </li>
              <li className="general-content-info-menu-item">
                <Card style={{ width: 200 }}>
                  <p className="general-content-item-text">  Your Children: <span>{children.length}</span></p>
                </Card>
              </li>
            </ul>
          </div>

        </div>
    );
};

export default GeneralContent;
