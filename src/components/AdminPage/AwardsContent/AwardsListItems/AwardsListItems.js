import React from "react";
import {Collapse} from "antd";
import './AwardsListItems.less';

const { Panel } = Collapse;

const AwardsListItems = (props) => {

    const {firstCourseDone, mathDone, englishDone, allCoursesDone} = props;

    return (
        <Collapse
            className="awards-list"
            bordered={false}
        >
            <Panel className="awards-list-panel"
                   header="Awards List"
                   key="1"
            >
                
                <ul className="awards-list-block">
                    <li className="awards-list-item">
                        <div
                            className={
                                `${(firstCourseDone) ? 'awards-list-item-image-active' : ''}
                                 awards-list-item-image
                                 awards-first-done`}
                        >
                        </div>
                    </li>
                    <li className="awards-list-item">
                        <div
                            className={
                                `${(englishDone) ? 'awards-list-item-image-active' : ''}
                                     awards-list-item-image
                                     awards-english-done`}
                        >
                        </div>
                    </li>
                    <li className="awards-list-item">
                        <div
                            className={
                                `${(mathDone) ? 'awards-list-item-image-active' : ''}
                                     awards-list-item-image
                                     awards-math-done`}
                        >
                        </div>
                    </li>
                    <li className="awards-list-item">
                        <div
                            className={
                                `${(allCoursesDone) ? 'awards-list-item-image-active' : ''}
                                     awards-list-item-image
                                     awards-all-done`}
                        >
                        </div>
                    </li>
                </ul>
            </Panel>
        </Collapse>
    );
};

export default AwardsListItems;