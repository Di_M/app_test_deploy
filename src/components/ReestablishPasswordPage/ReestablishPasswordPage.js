import React from 'react';
import './ReestablishPasswordPage.less'
import FormReestablish from './FormReestablish/FormReestablish';


const ReestablishPasswordPage = () => {

    return (

        <div className="reestablish-password">

            {/*Content*/}
            <div className="reestablish-password-content">

                {/*Title block*/}
                <h2 className="reestablish-password-title">
                    Reestablish Password
                </h2>

                {/*Form*/}
                <div className="reestablish-password-form-wrap">
                    <FormReestablish />
                </div>

            </div>
        </div>
    );
};

export default ReestablishPasswordPage;