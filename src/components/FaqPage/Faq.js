import React from 'react';
import './FaqPage.less';
import { Collapse } from 'antd';
import { CaretRightOutlined } from '@ant-design/icons';

const { Panel } = Collapse;

const FaqPage=props=>{
    return <React.Fragment>

        <section className="page-faq">

            <div className="page-faq-block">

                <h1 className="page-faq-block-title">FAQ</h1>
                <div className="page-faq-block-items">

                    <Collapse accordion
                              bordered={false}
                              className="page-faq-block-collapse"
                              expandIcon={({ isActive }) => <CaretRightOutlined rotate={isActive ? 90 : 0} />}
                              >
                        <Panel header="How to start?" key="1" className="page-faq-block-collapse-item">
                            <p className="page-faq-block-collapse-text">It is easy, just register as a parent in our system and choose a course for your child.</p>
                        </Panel>
                        <Panel header="How much cost of using your service?" key="2" className="page-faq-block-collapse-item">
                            <p className="page-faq-block-collapse-text">Our service is free. Anyone can use it for own needs/</p>
                        </Panel>
                        <Panel header="How many courses do you have? " key="3" className="page-faq-block-collapse-item">
                            <p className="page-faq-block-collapse-text">Currently we have 3 courses but only one available for usage.</p>
                        </Panel>
                        <Panel header="How long do I have to complete an Elementary Skills course? " key="4" className="page-faq-block-collapse-item">
                            <p className="page-faq-block-collapse-text">As noted above, there are no deadlines to begin or complete the course.
                                Even after you complete the course, you will continue to have access to it,
                                provided that your account’s in good standing, and ElementarySkills continues to have a license to the course.</p>
                        </Panel>

                        <Panel header="Is there any way to preview a course?" key="5" className="page-faq-block-collapse-item">
                            <p className="page-faq-block-collapse-text">Yes! If you're not sure if a course is right for you,
                                you can start a free preview and complete assignments in your chosen course.</p>
                        </Panel>

                        <Panel header="What if I don’t like a course?" key="6" className="page-faq-block-collapse-item">
                            <p className="page-faq-block-collapse-text">If you don't like our course - you can quit any time.</p>
                        </Panel>

                        <Panel header="Will I continue to have access to the course even after I complete it?" key="7"
                               className="page-faq-block-collapse-item">
                            <p className="page-faq-block-collapse-text">Yes. You will continue to have access to the course after you complete it.</p>
                        </Panel>

                        <Panel header="Do free courses offer lifetime access?" key="8" className="page-faq-block-collapse-item">
                            <p className="page-faq-block-collapse-text">Yes. Students also receive lifetime access to free courses, provided their account remains
                                in good standing and Elementary Skills continues to have a license to the course.</p>
                        </Panel>

                        <Panel header="Can I delete my account? " key="9" className="page-faq-block-collapse-item">
                            <p className="page-faq-block-collapse-text">Yes, you can delete your account- just contact our support team.</p>
                        </Panel>

                        <Panel header="Still have questions?" key="10" className="page-faq-block-collapse-item">
                            <p className="page-faq-block-collapse-text">If you have any other questions - please, feel free to contact us.</p>
                        </Panel>
                    </Collapse>

                </div>


            </div>

        </section>

    </React.Fragment>
}
export default FaqPage;