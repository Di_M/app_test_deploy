import React from 'react';
import './ContactsPage.less';
import { FacebookOutlined, TwitterOutlined, LinkedinOutlined } from '@ant-design/icons';
import { Form, Input, InputNumber, Button } from 'antd';


const ContactsPage=props=>{

    const layout = {
        labelCol: {
            span: 8,
        },
        wrapperCol: {
            span: 16,
        },
    };
    const validateMessages = {
        required: '${label} is required!',
        types: {
            email: '${label} is not validate email!',

        }
    };
    const onFinish = values => {
        console.log(values);
    };



    return <React.Fragment>

      <section className="page-contact-us">
          <h1 className="page-contact-us-title">Contacts</h1>
          <div className="page-contact-us-block">
              <p className="page-contact-us-sub-text">Please feel free to ask us any questions</p>
              <div className="page-contact-us-address">
                  <div  className="page-contact-us-address-content">
                      <p>735 Tehama Street San Diego, California, 94103 USA</p>
                  </div>

                  <div  className="page-contact-us-address-social">
                      <h3  className="page-contact-us-address-social-text">Follow us:</h3>
                      <a href="#" className="page-contact-us-social-link"><FacebookOutlined /></a>
                      <a href="#" className="page-contact-us-social-link"><TwitterOutlined /></a>
                      <a href="#" className="page-contact-us-social-link"><LinkedinOutlined /></a>
                  </div>

              </div>
              <div className="page-contact-us-form">
                      <h3 className="page-contact-us-form-title">Ask your question via our form</h3>
                  <div className="page-contact-us-form-body">

                      <Form {...layout} name="nest-messages" onFinish={onFinish} validateMessages={validateMessages} className="page-contact-us-form-body-form">
                          <Form.Item
                              name={['user', 'name']}
                              label="Name"
                              rules={[
                                  {
                                      required: true,
                                  },
                              ]}
                          >
                              <Input />
                          </Form.Item>
                          <Form.Item
                              name={['user', 'email']}
                              label="Email"
                              rules={[
                                  {
                                      type: 'email',
                                      required: true,
                                  },
                              ]}
                          >
                              <Input />
                          </Form.Item>
                          <Form.Item name={['user', 'message']} label="Message">
                              <Input.TextArea />
                          </Form.Item>
                          <Form.Item wrapperCol={{ ...layout.wrapperCol, offset: 8 }}>
                              <Button type="primary" htmlType="submit" className="contact-page-btn">
                                  Submit
                              </Button>
                          </Form.Item>
                      </Form>

                  </div>

              </div>
          </div>


      </section>
    </React.Fragment>
}
export default ContactsPage;