import React from 'react';
import LoginWithSocialButton from "../lib/Button/LoginWithSocialButton";
import {FacebookOutlined, GooglePlusOutlined} from "@ant-design/icons";
import './SignInPage.less'
import FormLogin from './FormLogin/FormLogin';


const SignInPage = () => {

    const gmail = <GooglePlusOutlined />;
    const facebook = <FacebookOutlined />;

    return (

        <div className="sign-in">

            {/*Content*/}
            <div className="sign-in-content">

                {/*Title block*/}
                <h2 className="sign-in-title">
                    Welcome
                </h2>

                {/*Form*/}
                <div className="sign-in-form-wrap">
                    <FormLogin />
                </div>

                {/*OR Text*/}
                <div className="sign-in-another-way-title">
                    <span>OR</span>
                </div>

                {/*Social block*/}
                <div className="sign-in-social">
                    <LoginWithSocialButton
                        title="Sign Up with Gmail"
                        icon={gmail}
                        href="#"
                        color="error"/>
                    <LoginWithSocialButton
                        title="Sign Up with Facebook"
                        icon={facebook}
                        href="#"
                        color="info"/>
                </div>
            </div>
        </div>
    );
};

export default SignInPage;