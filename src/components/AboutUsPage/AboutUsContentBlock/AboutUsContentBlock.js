import React from "react";
import './AboutUsContentBlock.less'
import about_us from './images/about_us.jpg'
import mission from './images/mission.jpg'

const AboutUsContentBlock =props=>{
    return (
        <React.Fragment>
            <section className="about-us-section">
                <div className="about-us-section-block">
                    <h1 className="about-us-section-title">About Us</h1>
                    <div className="about-us-section-block-content about-us-block-1">

                        <div className="about-us-section-block-content-image"><img src={about_us} alt="about us"/></div>

                        <div className="about-us-section-block-content-text">

                            <h1>Learning Better Together</h1>

                            <p>"Elementary Skills" brings everyone in the education community together to help learners succeed.
                                We create technology, content and platforms that connect teachers, students and parents to
                                each other and help all learners discover their passions and improve their skills.</p>
                        </div>

                    </div>

                    <div className="about-us-section-block-content about-us-block-2">

                        <div className="about-us-section-block-content-text">

                            <h1>Our mission</h1>

                            <p>Our goal is to connect all learners with the people and resources they need to reach their full potential.
                                Over the last ten years as the classroom itself has become so much more than a physical space,
                                we’ve developed some amazing technology that teachers love and depend on.
                                But we know we have so much more to do and we’re always looking ahead to how we can improve the
                                learning outcomes of entire communities.</p>
                        </div>
                        <div className="about-us-section-block-content-image"><img src={mission} alt="our mission"/></div>

                    </div>

                </div>


            </section>

        </React.Fragment>
    )
}

export default AboutUsContentBlock;