import React from 'react';
import './AboutUs.less';
import HomePageContactUsBlock from "../HomePage/HomePageContactUsBlock/HomePageContactUsBlock";
import AboutUsContentBlock from "./AboutUsContentBlock/AboutUsContentBlock"

const AboutUs=props=>{
    return <React.Fragment>
       <AboutUsContentBlock/>
        <HomePageContactUsBlock/>
    </React.Fragment>
}
export default AboutUs;