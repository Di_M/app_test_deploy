import Quiz from 'react-quiz-component';
import React from "react";
import './QuizComponent.less'
import {Button} from "antd";

export const quiz = require('./QuizJSON.json')

const renderCustomResultPage = (obj) => {
    console.log(obj);
    return (
        <div>
            This is a custom result page. You can use obj to render your custom result page
        </div>
    )
}

const onCompleteAction = (obj) => {
// added redux handler
    console.log(obj);

    return (<React.Fragment>
        <div className="quiz-answer-container">

            <div className="quiz-answer-container-wrapper">
            <div><h3>Congratulation! You have finished this course! </h3></div>
            <div className="quiz-answer-container-results">You Earned: <span className="quiz-answer-container-results-red">{obj.correctPoints}</span>  points from
                <span className="quiz-answer-container-results-green"> {obj.totalPoints}</span></div>
            <div className="quiz-answer-container-results">Correct Answers:<span className="quiz-answer-container-results-green"> {obj.numberOfCorrectAnswers}</span></div>
            <div className="quiz-answer-container-results">Incorrect Answers:<span className="quiz-answer-container-results-red"> {obj.numberOfIncorrectAnswers}</span></div>
            <div className="quiz-answer-container-results">Total Questions:<span className="quiz-answer-container-results-green">  {obj.numberOfQuestions}</span></div>
            <Button href="/course" className="quiz-answer-container-btn"  type="primary">Start Again</Button>
            </div>
        </div>
    </React.Fragment>)
}


const QuizComponent =(props)=>{

    return <React.Fragment>
        <Quiz quiz={quiz}  customResultPage={renderCustomResultPage} onComplete={onCompleteAction}/>
    </React.Fragment>
}
export default QuizComponent