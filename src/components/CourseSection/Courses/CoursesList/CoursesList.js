import React from 'react'
import CourseShortDescription from '../CourseShortDescription/CourseShortDescription'
import './CoursesList.less'
import fakeCourseData from '../CourseFakeData'


const CoursesList =(props)=>{


   const {courseId, courseImg, courseTitle, courseGrade } =fakeCourseData

    return ( <React.Fragment>
        <div className="courses-list-block">
            <div className="courses-list-block-wrapper">

       <h3> Courses List</h3>

        <CourseShortDescription key={courseId} courseImg={courseImg} courseTitle={courseTitle} courseGrade={courseGrade} />
            <CourseShortDescription key={courseId} courseImg={courseImg} courseTitle={courseTitle} courseGrade={courseGrade} />
            <CourseShortDescription key={courseId} courseImg={courseImg} courseTitle={courseTitle} courseGrade={courseGrade} />
            </div>
        </div>
    </React.Fragment>)
}
export default CoursesList