import React,  { useState, useEffect }from 'react'
import {Link, withRouter} from "react-router-dom";
import LessonTheoryBlock from '../LessonTheoryBlock/LessonTheoryBlock'
import './Lesson.less'
import {EditOutlined, CheckOutlined} from '@ant-design/icons'
import {lessonFakeData} from '../../../../services/helpers/LessonFakeData'

import {Tooltip, Breadcrumb, Button} from "antd";
import LessonQuestionBody from "../LessonQuestionBody/LessonQuestionBody";
import LessonAnswerBlock from "../LessonAnswerBlock/LessonAnswerBlock";
import { useSelector } from "react-redux";

import {getOneLesson, postLessonAnswer} from '../../../../services/lessonApi/lessonApi'
//Temp
import useUpdateLessonData from '../../../../services/hooks/updateLessonData'

// GET DATA From Server


const Lesson =(props)=>{


    console.log ('LESSON COMPONENT IS RENDERING')

    // const getLessonIdData = useSelector(store => store.LessonIdReducer);
    // const userID = useSelector(store => store.UserIDReducer);
    // console.log('LESSON COMPONENT, LESSON DATA:', getLessonIdData)
    // const lessonIdArray = getLessonIdData.lessonIdList;
    // //get value from URL
    // const lessonUrlId=parseInt(props.match.params.id);
    // const lessondbId =lessonIdArray[lessonUrlId]
    //
    // //TEMP
    // const getCourseDetails  = useSelector(store => store.CourseDetailsReducer)
    // console.log('LESSON COMPONENT, COURSE-DETAILS:', getCourseDetails)
    // const courseData = getCourseDetails.courseDetails
    // const rowDataSection1= courseData.courseSections[0].courseSectionLessons;
    // const rowDataSection2= courseData.courseSections[1].courseSectionLessons;
    // const rowData = rowDataSection1.concat(rowDataSection2);
    //Moved to HOOK:


    const {lessonIdArray, rowData} = useUpdateLessonData()

    const lessonUrlId=parseInt(props.match.params.id);
    const lessondbId =lessonIdArray[lessonUrlId]

    const userID = useSelector(store => store.UserIDReducer);

     //filter array with the right lesson in order to get a lesson object => get the first element of filtered array
     let LessonData = (rowData.filter(el=>{return (el._id===lessondbId)}) )[0]

    if (LessonData===undefined){
        console.error('ERROR: Lesson Data in Lesson.js component is Undefined, was used DEFAULT VERSION')
        LessonData = lessonFakeData;
    } else( console.log( 'Current LessonDATA is:  ', LessonData))


    const [userAnswer, setUserAnswer] = useState(false)

    // useEffect(() => {
    //     setUserAnswer(false)
    // },[]);



// add Redux Handler!

   let {lessonImg,
       title,
       lessonPoints,
       description,
       exampleBody,
       questionBody,
       questionTitle,
       lessonType,
       questionType,
       questionAnswer}=LessonData;


    const checkLessonQuestionAnswer =(status)=> {
        if (status===true){
            setUserAnswer(status)
            //lessonIsFinished => true
            //changed Points Number
            // Changed % of Finished Courses => function()
        } else {
            setUserAnswer(false)
        }
    }

    const nextBtnHandler=(lessonId)=>{
        if (lessonId<20){
            return(`/account/${userID.userID}/courses/math/lesson/${lessonUrlId+1}`)
        }else {
            return(`/account/${userID.userID}`);
        }
    }


    return (
    <React.Fragment>
        <div className="lesson-block">
            <div className="lesson-block-breadcrumb">
            <Breadcrumb>
                <Breadcrumb.Item><Link to={`/account/${userID.userID}/`}>Main</Link></Breadcrumb.Item>
                <Breadcrumb.Item>
                    <Link to={`/account/${userID.userID}/courses/math`}>Math Course</Link>
                </Breadcrumb.Item>
                <Breadcrumb.Item>Lesson:{title}</Breadcrumb.Item>
            </Breadcrumb>
            </div>

            <div className="lesson-block-wrapper">
            <div className="lesson-block-header">
                <img src={lessonImg} className="lesson-block-img" alt="lesson img"/>
                <h4 className="lesson-block-title">{title}</h4>

                <Tooltip
                    trigger={['hover']}
                    title="How many points you can earn for the lesson"
                    placement="topLeft"
                    overlayClassName="numeric-input">
                <h4 className="lesson-block-points">Points: <span>{lessonPoints}</span></h4>
                </Tooltip>

                <Tooltip
                    trigger={['hover']}
                    title="Lesson Status: Finished or Not"
                    placement="topLeft"
                    overlayClassName="numeric-input">
                {userAnswer?<div className="lesson-block-status lesson-block-finished"><CheckOutlined />
                       </div>: <div className="lesson-block-status lesson-block-started"> <EditOutlined /></div>}
                </Tooltip>
            </div>

            <div className="lesson-block-theory">
                <LessonTheoryBlock description={description}
                                   exampleBody={exampleBody}
                                   type={lessonType}/>
            </div>
            <div className="lesson-block-practice">
                <LessonQuestionBody   questionBody={questionBody}
                                      questionTitle={questionTitle} questionType={questionType}/>
                 <LessonAnswerBlock questionAnswer={questionAnswer}
                                    checkLessonQuestionAnswer={checkLessonQuestionAnswer}
                                    userAnswer={userAnswer}/>
                {userAnswer?<Link to={nextBtnHandler(lessonUrlId)}>
                    <Button type="primary" onClick={()=>{checkLessonQuestionAnswer(false)}}>Next Lesson</Button>
                </Link>:null}

            </div>
            </div>
        </div>

    </React.Fragment>)
}

export default withRouter(Lesson)