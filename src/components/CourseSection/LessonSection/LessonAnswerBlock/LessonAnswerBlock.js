import React, {useState, useEffect} from 'react'
import './LessonAnswerBlock.less'
import {Button, Input, Tooltip} from "antd";
import textMessages from '../../Courses/Helpers/ConstMessages';

const LessonAnswerBlock =(props)=>{
    const {type, rightAnswer, rightMessage, IsAnswered} = props.questionAnswer
    const {userAnswer,checkLessonQuestionAnswer} = props
    const {wrongAnswer} = textMessages


    const [inputValue, setInputValue] = useState('')
    const[wrongTriggerMessage, setWrongTriggerMessage] = useState(false)

    // useEffect(() => {
    //     checkLessonQuestionAnswer(false)
    // },[]);



    const CheckAnswerBtnHandler = ()=>{
        console.log(inputValue)
        console.log(rightAnswer.toString())
        if((inputValue)===rightAnswer.toString()){
            checkLessonQuestionAnswer(true)
            setWrongTriggerMessage(false)
        } else {
            setWrongTriggerMessage(true)

        }
    }


    return <React.Fragment>
        <div className="lesson-practice-answer-block-wrapper">

        <div className="lesson-practice-answer-block">
            {!userAnswer?<React.Fragment>
            <Tooltip
                trigger={['focus']}
                title="Enter an answer"
                placement="topLeft"
                overlayClassName="numeric-input">
                <Input
                    className="lesson-practice-answer-input"
                    placeholder="Enter an answer"
                    name="answer"
                    maxLength={25}
                    onChange={event => setInputValue(event.target.value)}
                />
            </Tooltip>

            <Button type="primary" className="lesson-practice-answer-btn" onClick={CheckAnswerBtnHandler}>My Answer</Button></React.Fragment>: <div className="lesson-practice-answer-message">{rightMessage}</div>}

        </div>
            {wrongTriggerMessage?<div className="lesson-practice-answer-message-wrong">{wrongAnswer}</div>:null}
        </div>
    </React.Fragment>
}

export default LessonAnswerBlock