import React, { useState }from 'react';

import { Input, Tooltip, Button } from 'antd';
import './LessonQuestionBody.less'
import LessonExampleBlockItem from "../LessonTheoryBlock/LessonExampleBlockItem";
const uniqid = require('uniqid');

const LessonQuestionBody =(props)=>{
    const {questionTitle, questionBody, questionType}=props


    let questionBlock

    switch(questionType){
        case "counting":{
            questionBlock = (<div>
                {questionBody.map(item=>{
                    return( <LessonExampleBlockItem lesson={item} key={uniqid()} question={item.question} type={item.type}/>)
                })
                }
            </div>)
            break
        }
        case "compare": {
            questionBlock = (<div>
                {questionBody.map(item => {
                    return (
                        <LessonExampleBlockItem lesson={item} key={uniqid()}  displayAnswer={item.displayAnswer} type={item.type}
                                                symbol={item.symbol}/>)
                })
                }
            </div>)
            break

        }
        default: {
            questionBlock = <div><p>Oops..This question does't have conditions.</p></div>
        }
    }



    return (
        <React.Fragment>
            <div className="lesson-practice-wrapper">
                <h3>Practice:</h3>
                <h4>{questionTitle}</h4>
                <div className="lesson-practice-body">
                    {questionBlock}
                </div>
            </div>


    </React.Fragment>)
}
export default LessonQuestionBody