import React from 'react'
import LessonExampleBlockItem from './LessonExampleBlockItem'
import './LessonTheoryBlock.less'
const uniqid = require('uniqid');

const LessonTheoryBlock =(props)=>{
    const {description, exampleBody, type} = props

    let displayBlock;

// scenarios for different types of questions:

    switch (type){
        case "counting": {
            displayBlock = (<div className="lesson-theory-type-counting">
                {exampleBody.map(item => {
                        return (<LessonExampleBlockItem lesson={item} key={uniqid()}/>)
                    }
                )}

            </div>)
            break
        }
        case "compare":{
            displayBlock = (<div className="lesson-theory-type-counting">
            {exampleBody.map(item => {
                    return (<LessonExampleBlockItem lesson={item} key={uniqid()}/>)
                }
            )}

        </div>)
            break


        }
        default:
            displayBlock = <div><p>Oops..This lesson does't have examples.</p></div>
    }



 return(
     <React.Fragment>

         <div className="lesson-theory-wrapper">
             <h3 className="lesson-theory-title">Theory:</h3>
             <p className="lesson-theory-description">{description}</p>
             <div className="lesson-theory-example-wrapper">
                 {displayBlock}
             </div>
         </div>

 </React.Fragment>)
}
export default LessonTheoryBlock
