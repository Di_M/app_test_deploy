import * as types from '../constants/constTypes';

const initState = {
    creditCard: {},
    added: false
};

function CreditCardReducer(state = initState, action) {

    switch (action.type) {

        case types.GET_CREDIT_CARD:
            return {
                ...state,
                ...{
                    creditCard: action.payload,
                    added: true
                }
            };
        case types.DELETE_CREDIT_CARD:
            return {
                ...initState
            };

        default:
            return state
    }
}

export default (CreditCardReducer);