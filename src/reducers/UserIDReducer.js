import * as types from '../constants/constTypes';

const initState = {
    userID: null,
};

function UserIDReducer(state = initState, action) {

    switch (action.type) {

        case types.GET_USER_ID:
            return {
                ...state,
                ...{userID: action.payload}
            };
        case types.DELETE_USER_ID:
            return {
                ...initState
            };

        default:
            return state
    }
}

export default (UserIDReducer);