import * as types from '../constants/constTypes';

const initState = {
    role: null
};

function RoleReducer(state = initState, action) {

    switch (action.type) {

        case types.GET_ROLE:
            return {
                ...state,
                ...{role: action.payload}
            };

        case types.DELETE_ROLE:
            return {
                ...initState
            };

        default:
            return state
    }
}

export default (RoleReducer);