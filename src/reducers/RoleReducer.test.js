import * as types from '../constants/constTypes';
import RoleReducer from './RoleReducer';

describe('Role Reducer', () => {

    describe('Test Initial state', () => {

        it('Should render initial state', () => {
            const newState = RoleReducer(undefined, {});
            expect(newState).toEqual({
                role: null
            });
        });

    });

    describe('Test New state', () => {

        let someRole;

        beforeEach(() => {
            someRole = 'parent';
        });

        it('Should return new state if receiving type GET_ROLE', () => {
            const newState = RoleReducer(undefined, {
                type: types.GET_ROLE,
                payload: someRole
            });
            expect(newState).toEqual({
                role: someRole,
            });
        });

        it('Should return initState if receiving type DELETE_ROLE', () => {
            RoleReducer(undefined, {
                type: types.GET_ROLE,
                payload: someRole
            });
            const finallyState = RoleReducer(undefined, {
                type: types.DELETE_ROLE,
            });
            expect(finallyState).toEqual({
                role: null
            });
        });

    });


});