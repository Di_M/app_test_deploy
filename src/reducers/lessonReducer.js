import * as types from '../constants/constTypes';
import {lessonFakeId} from '../services/helpers/LessonFakeId'
const initState = {
   lessonIdList: lessonFakeId,
    updated: false
};

function LessonIdReducer(state = initState, action) {

    switch (action.type) {

        case types.GET_LESSON_IDS:
            return {
                ...state,
                ...{
                    lessonIdList: action.payload,
                    updated: true
                }
            };

        case types.CLEAR_LESSON_IDS:
            return {
                ...initState
            };

        default:
            return state
    }
}

export default (LessonIdReducer);