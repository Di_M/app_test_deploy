import * as types from '../constants/constTypes';
import TokenReducer from './TokenReducer';

describe('Token Reducer', () => {

    describe('Test Initial state', () => {

        it('Should render initial state', () => {
            const newState = TokenReducer(undefined, {});
            expect(newState).toEqual({
                token: null,
                auth: false
            });
        });

    });

    describe('Test New state', () => {

        let someToken;

        beforeEach(() => {
            someToken = 'sfsd@sfsdf33434';
        });

        it('Should return new state if receiving type GET_TOKEN', () => {
            const newState = TokenReducer(undefined, {
                type: types.GET_TOKEN,
                payload: someToken
            });
            expect(newState).toEqual({
                token: someToken,
                auth: true
            });
        });

        it('Should return initState if receiving type DELETE_TOKEN', () => {
            TokenReducer(undefined, {
                type: types.GET_TOKEN,
                payload: someToken
            });
            const finallyState = TokenReducer(undefined, {
                type: types.DELETE_TOKEN,
            });
            expect(finallyState).toEqual({
                token: null,
                auth: false
            });
        });

    });


});