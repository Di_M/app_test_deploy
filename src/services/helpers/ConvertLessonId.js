import React from 'react';


const convertLessonId=(object)=>{
 if(object !==undefined){
        let arrayOfLessonId = []
        const firstSectionLessons = (object.courseSections[0]).courseSectionLessons
        const secondSectionLessons = (object.courseSections[1]).courseSectionLessons
        let lessonsArray
        if (firstSectionLessons[0]._id !==undefined){
            lessonsArray = [...firstSectionLessons, ...secondSectionLessons]
            arrayOfLessonId =lessonsArray.map(item=>item._id)
            return arrayOfLessonId

        }

    }



}

export default convertLessonId