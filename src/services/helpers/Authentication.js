import LocalStorage from "../LocalStorageRequest";
import * as TokenActions from "../../actions/TokenActions";
import * as UserIDActions from "../../actions/UserIDActions";
import * as RoleActions from "../../actions/RoleActions";
import {useDispatch} from "react-redux";

const Authentication = () => {

    const dispatch = useDispatch();
    const token = LocalStorage.getData('token');
    const role = LocalStorage.getData('role');
    const userID = LocalStorage.getData('userID');

    if(token) {
        dispatch(TokenActions.getToken(token));
    } else {
        return false;
    }

    if(userID) {
        dispatch(UserIDActions.getUserID(userID));
    } else {
        return false;
    }

    if(role && role === 'parent' || role && role === 'child') {
        dispatch(RoleActions.getRole(role));
    } else {
        return false;
    }

    return true;

};

export default Authentication;