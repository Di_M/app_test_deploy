import request from "../request";
import { url } from "../../constants/constURL";

const {
    axiosInstance,
    getApiRequestConfig
} = request;

export const sendUserLogIn = ({name, password, remember, role}) => axiosInstance
    .post(url.signIn, {
        name,
        password,
        remember,
        role,
    }, getApiRequestConfig());

export const sendUserReg = ({name, email, password}) => axiosInstance
    .post(url.signUP, {
        name,
        email,
        password,
    }, getApiRequestConfig());


// GET request to 'http://localhost:4004/account/userID' to receive user data. UserId should be provided as a parameter.
export const getUserInfo = (id) => axiosInstance
  .get(`${url.userInfo}/${id}`, getApiRequestConfig());

export const getUserChildren = (id) => axiosInstance
  .get(`${url.userInfo}/${id}/children`, getApiRequestConfig());


export const putParentPassword = ({password, oldPassword}) => axiosInstance
    .put(url.changePassword, {
        password,
        oldPassword,
    }, getApiRequestConfig());
