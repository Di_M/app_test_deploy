import request from "../request";
import { url } from "../../constants/constURL";
const {
    axiosInstance,
    getApiRequestConfig
} = request;

// POST request to 'http://localhost:4004/course/list' to get List of Courses
//old (Currently working)
export const getCourseListRequest = async function() {
    try {
       return await axiosInstance
            .get(`${url.coursesList}`, getApiRequestConfig())
            .then(response=>response.data.courses)

    } catch (error){
        console.error(error)
    }
};

export const getCourseDetailsRequest = async function(){
    try {
        return await axiosInstance
            .get(`${url.coursesDetails}`, getApiRequestConfig())
            .then(response=>response.data.courses)

    } catch (error){
        console.error(error)
    }
};



//new
//
// export const getFullCoursesList = () => {
//     try {
//         axiosInstance.get((`${url.fullCoursesList}`), getApiRequestConfig());
//     } catch (error) {
//         console.log(error)
//     }
// }




