export default class LocalStorage {

    static getData(dataName){
        const arrayData = localStorage.getItem(dataName);

        return JSON.parse(arrayData);
    }

    static setData(dataName, data) {
        const stringData = JSON.stringify(data);

        localStorage.setItem(dataName, stringData);
    }

    static clearData(dataName) {
        let emptyData = [];
        this.setData(dataName, emptyData);
    }

    static clearAllData() {
        localStorage.clear();
    }
}