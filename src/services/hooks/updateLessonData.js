
import {useSelector} from "react-redux";
function useUpdateLessonData(){

    const getLessonIdData = useSelector(store => store.LessonIdReducer);
    const lessonIdArray = getLessonIdData.lessonIdList;

    //TEMP
    const getCourseDetails  = useSelector(store => store.CourseDetailsReducer)
    const courseData = getCourseDetails.courseDetails
    const rowDataSection1= courseData.courseSections[0].courseSectionLessons;
    const rowDataSection2= courseData.courseSections[1].courseSectionLessons;
    const rowData = rowDataSection1.concat(rowDataSection2);

 return {lessonIdArray, rowData}
}
export default useUpdateLessonData