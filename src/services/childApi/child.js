import request from "../request";
import { url } from "../../constants/constURL";

const {
  axiosInstance,
  getApiRequestConfig
} = request;

// POST request to 'http://localhost:4004/add-child' to register new child. Child data should be provided
export const sendChildReg = ({name, age, gender, password}) => axiosInstance
  .post(url.signUpChild, {
    name,
    age,
    gender,
    password,
  }, getApiRequestConfig());


export const assignCourseToChildren = (assignedChildrenList, courseId) => axiosInstance
  .put(url.assignCourse, {
    assignedChildrenList,
    courseId,
  }, getApiRequestConfig());

export const reassignCourseFromChild = (childId, courseId) => axiosInstance
  .put(url.reassignCourse, {
    childId,
    courseId,
  }, getApiRequestConfig());

export const deleteChildRequest = (childId) => axiosInstance
    .delete(url.deleteChild + childId, getApiRequestConfig());

export const editChildRequest = ({id, name, age, gender}) => axiosInstance
    .put(url.editChild + id, {
        name,
        age,
        gender
    }, getApiRequestConfig());
