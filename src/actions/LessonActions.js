import React from 'react'
import * as types from '../constants/constTypes';

export const addLessonIdList = (ids) => {
        return dispatch => {
            dispatch({
                type: types.GET_LESSON_IDS,
                payload: ids
            });
        };
};

export const clearLessonIdList = () => {
    return dispatch => {
        dispatch({
            type: types.CLEAR_LESSON_IDS
        });
    };
};

