import * as types from '../constants/constTypes';

export function getCreditCard(list) {

    return dispatch => {
        dispatch({
            type: types.GET_CREDIT_CARD,
            payload: list
        });
    };
}

export function deleteCreditCard() {

    return dispatch => {
        dispatch({
            type: types.DELETE_CREDIT_CARD
        });
    };
}