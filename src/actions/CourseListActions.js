import React from 'react'
import * as types from '../constants/constTypes';



export const displayCourseList = (courses) => {
    return dispatch => {
    dispatch({
      type: types.SHOW_COURSE_LIST,
      payload: courses
    });
  };
};

export function clearCourseList() {

    return dispatch => {
        dispatch({
            type: types.CLEAR_COURSE_LIST,
        });
    };
}

