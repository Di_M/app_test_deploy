import * as types from '../constants/constTypes';

export function getRole(list) {

    return dispatch => {
        dispatch({
            type: types.GET_ROLE,
            payload: list
        });
    };
}

export function deleteRole() {

    return dispatch => {
        dispatch({
            type: types.DELETE_ROLE,
        });
    };
}